if (process.env.NODE_ENV !== "production") {
  require('dotenv').config()
}

const stripeSecretKey = process.env.STRIPE_SECRET_KEY
const stripePublicKey = process.env.STRIPE_PUBLIC_KEY

const express = require('express')


// this creates a new app variable which is equal to creating server http.createServer()
const app = express()

const fs = require('fs')

// allows us to embed server side code inside our html pages
app.set('view engine', 'ejs')

// to tell our app were our public FE facing files are. express.static marks all files in public as static.
app.use(express.static('public'))


// to set route to store page
app.get('/store', function(req, res) {
fs.readFile('items.json', function(error, data) {
  if (error) {
    res.status(500).end()
  } else {
    res.render('store.ejs', {
      items: JSON.parse(data)
    })
  }
})
})

//port we want to listen
app.listen(7000)


